package adapter

import (
	"context"
	"log"
	"time"

	"google.golang.org/grpc"
	pb "shawgeek.com/sgitech-weather/api/weathertracker"
)

// WeatherTrackerAdapter abstracts the gRPC client and connection from the CLI Client
type WeatherTrackerAdapter struct {
	Connection *grpc.ClientConn
	Client     pb.WeatherTrackerClient
}

// NewWeatherTrackerAdapter returns a new adapter for the given connection
func NewWeatherTrackerAdapter(connection *grpc.ClientConn) *WeatherTrackerAdapter {
	client := pb.NewWeatherTrackerClient(connection)
	wta := &WeatherTrackerAdapter{
		Connection: connection,
		Client:     client,
	}
	return wta
}

// AddLocationTemperature calls the server func to store a temperature for the given location
func (wt *WeatherTrackerAdapter) AddLocationTemperature(location string, temperature int32) (string, error) {
	log.Println("adapter.AddLocationTemperature")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	req := &pb.AddLocationTemperatureRequest{
		LocationName: location,
		TemperatureK: temperature,
	}

	r, err := wt.Client.AddLocationTemperature(ctx, req)

	if err != nil {
		log.Fatalf("adapter.AddLocationTemperature could not add temp: %v", err)
	}
	msg := r.GetMessage()

	return msg, err
}

// GetLocationTemperature calls the server func to get the location temperature for the given scale
func (wt *WeatherTrackerAdapter) GetLocationTemperature(location string, scale string) (int32, error) {
	log.Println("adapter.GetLocationTemperature")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	// Convert to Enum
	scaleEnumValue, ok := pb.GetLocationTemperatureRequest_TemperatureScale_value[scale]
	if !ok {
		panic("invalid enum value")
	}
	scaleEnum := pb.GetLocationTemperatureRequest_TemperatureScale(scaleEnumValue)

	req := &pb.GetLocationTemperatureRequest{
		LocationName: location,
		Scale:        scaleEnum,
	}

	r, err := wt.Client.GetLocationTemperature(ctx, req)

	if err != nil {
		log.Fatalf("adapter.GetLocationTemperature could not get temp: %v", err)
	}
	temperature := r.GetTemperature()

	return temperature, err
}
