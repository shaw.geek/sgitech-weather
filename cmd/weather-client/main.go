// Package main implements a client for Weather service.
package main

import (
	"flag"
	"log"
	"os"
	"strconv"

	"shawgeek.com/sgitech-weather/cmd/weather-client/adapter"

	"google.golang.org/grpc"
)

func main() {
	log.Println("Start Client")
	add := flag.Bool("add", false, "Add a Temerpature for a location")
	get := flag.Bool("get", false, "Get a temperature for a location")
	flag.Parse()
	// log.Printf("Add:[%v] Get:[%v]\n", *add, *get)

	adapter := initAdapter()
	defer adapter.Connection.Close()

	if *add == true {
		addLocationTemperature(adapter)
	} else if *get == true {
		getLocationTemperature(adapter)
	}

	log.Println("End Client")
}

func initAdapter() *adapter.WeatherTrackerAdapter {
	address := os.Getenv("WEATHER_TRACKER_ADDRESS")
	log.Printf("URL: [%s]", address)

	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	// Get New Adapter
	adapter := adapter.NewWeatherTrackerAdapter(conn)
	if err != nil {
		log.Fatalf("couldn't create adapter: %v", err)
	}

	return adapter
}

func addLocationTemperature(adapter *adapter.WeatherTrackerAdapter) {
	args := flag.Args()
	if len(args) != 2 {
		log.Fatalf("action 'add' requires location and Temperature")
	}
	name := args[0]
	temp, err := strconv.Atoi(args[1])

	if err != nil {
		log.Fatalf("bad param for action 'add': Temperature was not an int %s; %+v", args[1], err)
	}

	// Contact the server and print out its response.
	msg, err := adapter.AddLocationTemperature(name, int32(temp))
	if err != nil {
		log.Fatalf("could not add: %v", err)
	}
	log.Printf("Msg: [%s]", msg)
}

func getLocationTemperature(adapter *adapter.WeatherTrackerAdapter) {
	args := flag.Args()
	if len(args) != 2 {
		log.Fatalf("action 'get' requires location and scale")
	}
	name := args[0]
	scale := args[1]

	// Contact the server and print out its response.
	Temperature, err := adapter.GetLocationTemperature(name, scale)
	if err != nil {
		log.Fatalf("could not get: %v", err)
	}
	log.Printf("Temperature: [%d][%s]", Temperature, scale)
}
