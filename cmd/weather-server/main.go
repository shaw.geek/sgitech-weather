package main

import (
	"log"
	"net"
	"os"

	pb "shawgeek.com/sgitech-weather/api/weathertracker"
	"shawgeek.com/sgitech-weather/pkg/repository/dummy"
	rr "shawgeek.com/sgitech-weather/pkg/repository/redis"
	sv "shawgeek.com/sgitech-weather/pkg/service"

	"google.golang.org/grpc"
)

func main() {
	// use env variables so the config can be handled in deployment
	port := os.Getenv("WEATHERTRACKER_SERVER_PORT")

	log.Printf("WeatherTracker Server listening on port: [%s]", port)
	port = ":" + port
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("WeatherTracker Server failed to listen: %v", err)
	}
	s := grpc.NewServer()

	var wtRepo sv.WeatherTrackerRepository
	log.Println(os.Args)
	if len(os.Args) == 2 && os.Args[1] == "dummy" {
		// Use a Dummy Repo
		wtRepo, err = dummy.NewDummyRepository()
	} else {
		// Use a Redis Repo
		redisURL := os.Getenv("REDIS_URL")
		wtRepo, err = rr.NewRedisRepository(redisURL)
		if err != nil {
			log.Fatalf("failed to create new Redis repo: %v", err)
		}
	}

	log.Printf("Repo Type: [%T]", wtRepo)

	// Get a new tracker service, pass in the Repo - since we are using an interface, it could be any repo we implemented
	weatherSvc := sv.NewWeatherTracker(wtRepo)

	// Register it
	pb.RegisterWeatherTrackerServer(s, weatherSvc)

	// Serve it up
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve WeatherTracker: %v", err)
	}
}
