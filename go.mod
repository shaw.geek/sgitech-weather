module shawgeek.com/sgitech-weather

go 1.14

require (
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/golang/mock v1.4.3
	github.com/golang/protobuf v1.3.3
	github.com/pkg/errors v0.9.1
	google.golang.org/grpc v1.28.0
)
