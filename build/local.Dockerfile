# Start from the latest golang base image
FROM golang:latest as builder

# Add Maintainer Info
LABEL maintainer="Ryan Shaw <shaw.geek@gmail.com>"

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy go mod and sum files and download all dependencies
COPY go.mod go.sum ./
RUN go mod download

# Copy the source from the current directory to the Working Directory inside the container
COPY api/weathertracker api/weathertracker
COPY pkg pkg
COPY cmd/weather-server cmd/weather-server

# Build Weatherserver go app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o weatherserver cmd/weather-server/main.go


######## Start a new stage #######
FROM alpine:latest  

RUN apk --no-cache add ca-certificates

WORKDIR /root/

# Copy weatherserver go app from the previous stage
COPY --from=builder /app/weatherserver .

# Expose port 50505 to the outside the container
EXPOSE 50505
ENV WEATHERTRACKER_SERVER_PORT=:50505

# Command to run the executable
CMD ./weatherserver dummy | env