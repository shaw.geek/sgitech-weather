# SGITech Weather Tracker Microservice
A simple demonstration of a gRPC Microservice hosted in GCP using Kubernetes. It aligns with the 12Factor App design principles.

## Weather Tracker Microservice
The microservice has two methods 
* Store a Location Temperature in Kelvin
* Retrieve a Location Temperature in the requested scale

## gRPC and Protobuf
Leveraging protobuf, a developer can generate both client and server code for the gRPC methods and avoid writing the standard boilerplate code.

## Continuous Integration/Deployment
Using Docker and Kubernetes, CI builds can be kicked off and initiate a deployment into GPC using Kubernetes deployment manifests

### Docker Build
* Dockerfile for Weather Tracker Server to run in a lightweight alpine container
* Script to use gcloud and the dockerfile to build the docker image for the Weather Tracker Server and make it available in GCP for deployments

### Kubernetes Deployment Manifests
* Deployment for Redis master backend
* Service for the Redis master backend, allowing other pods to communicate to it
* (Optional) LoadBalancer for the Redis master service, optionally used for alternative configurations that require access to the Redis service outside the cluster.
* Deployment for the Weather Tracker Microservice
* LoadBalancer for Weather Tracker to expose it to the world
* ConfigMap to setup the environment variables for Weather Tracker and the Redis backend service

## Weather Tracker CLI Client
A CLI client that uses an environment variable to configure the address of the Weather Tracker Service
* `-add` [location name] [temperature kelvin] - Stores the location and temperature in Kelvin scale
* `-get` [location name] [scale] - Gets the temperature for the name in the requested scale

## 12Factor App Impacts
### 1: Code Base
* One code repo in github
* Code can be deployed in multiple environments including locally. Can run the app local or in a docker container or in GCP

### 2: Dependencies
* Dependencies are being managed with Go Modules
* I refrained from committing the dependent go packages. The violation of this factor has the following impacts:
* * If the package repositories are unreachable, the application will not build
* * There is a possibility that the packages may not be compatible or rely on different versions of the same package
* For the GCP deployment, the app is packaged into a Docker image and stored in GCP's container repository, this at least guarantees the same code and dependencies for any deployment of that image.

### 3: Config
* Configuration that changes per environment is exposed via environment variables and managed through a ConfigMap and Scripts
* There are no sensitive values in this application, but if there were, they would be used as environment variables controlled by Kubernetes Secrets.
* The only hard coded values per say are in the function "WeatherTracker.convertKelvin". These are scientific scalar values that do not change and therefore do not violate this factor.

### 4: Backing Services
* It's use of Redis is via URL as an attached resource. It does not care if Redis is local, in the same GCP cluster or hosted in a different cloud technology all together.

### 5: Build, release, run
* The creation of the docker image by gcloud is the build step
* The release step is handled in the kubectl apply deployments which are combining the build with the configuration that is handled in the ConfigMap
* The run stage is automatically handled by Kubernetes when it schedules/restarts/manages the pods.

### 6: Processes
* The app does not have state. It persists the location / temperature combinations in a Redis backend store. The pods can be deleted or scaled at will, with no regard to the Redis backend.

### 7: Port binding
* The weather tracker server is coded to look for an environment variable to define what port it is listening on. That port is provided via ConfigMap. 

### 8. Concurrency
 * The Weather Tracker Server is separate from the Redis backend and from the Weather Tracker Client. If Weather Tracker Server is overrun, we can scale it out without scaling Redis and vice-versa
 * Using Kubernetes to manage processes scale. It can be configured to auto scale if needed.

### 9. Disposability
* You can delete pods at will and they will be recreated with little to no impact to the client.
* Redis is handled as a backing service, helping to enable this behavior
* Environment Variables are handled in the ConfigMap

### 10. Environment parity
* The code is packaged into a Docker image and the infrastructure is defined in the Kubernetes Cluster and Deployment files. This __allows__ all the environments, including a developer's machine, to have app stack shape that is identical. 
* Using GCP Container Registry to guarantee that a build image can be shared and used by others with parity

### 11. Logs
* Important events and some tracing are being output to standard logger. Kubernetes gathers all of them from the pods and you can setup a log shipper to centralize them and configure a viewer to see the events you are interested in. 
* For brevity, I didn't setup anything, but I could have used Fluentd to ship the logs to Elasticsearch where I can search/analyze the logs and then use Kibana to visualize the results.

### 12. Admin processes
* This app does not have any need for one, but lets take a hypothetical scenario.
* If the Weather Tracker stored a history of Location/Temperatures, we could write an admin task that uses CronJob to run nightly to delete anything older than a configured number of days. 


## Expand on this service to allow for the use of an eventstore
* Create a GCP Spanner instance
* Create a new implementation of WeatherTrackerRepository interface that interacts with the Spanner Database
* Add an ENV variable for the WeatherTracker Server to decide which repo to use
* Poll the Spanner DB and publish events to a PubSub instance.
* Consider using stateless Cloud Functions when handling the tasks the events require

## How would this service be accessed and used from an external client from the cluster?
* Make sure the service configuration is a LoadBalancer so that endpoint is exposed outside the cluster. The current WeatherTracker service configuration for GKE is a LoadBalancer
* Use the proto file and an appropriate plugin to generate the client code for whatever programming language the client is going to use.