#!/bin/bash

# Parts of this could be executed by whatever Build/CI tooling you use. 


### Build Stage ###
# build Docker image from local code
gcloud builds submit --tag gcr.io/shawgeek-test/weathertrackerserver-image
# one option for CI is you could package the code up and push it to GCP Storage and build it from there


### Release Stage ###
# setup a config map for env variables
kubectl apply -f deployments/gke-configmap.yaml

# setup a Redis
kubectl apply -f deployments/gke-redis-master.yaml
kubectl apply -f deployments/gke-redis-slave.yaml

# setup Redis Service so Weather tracker can use it
kubectl apply -f deployments/gke-redis-slave-service.yaml
kubectl apply -f deployments/gke-redis-master-service.yaml

# deploy the weather tracker microservice
kubectl apply -f deployments/gke-deploy-weathertracker.yaml

# setup a service/load balancer to expose it to the world
kubectl apply -f deployments/gke-deploy-weathertracker-loadbalancer.yaml


### Run Stage ###
# Kubernetes takes care of the run by scheduling pods

