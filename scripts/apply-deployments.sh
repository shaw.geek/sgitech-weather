#!/bin/bash

# setup a config map for env variables
kubectl apply -f deployments/gke-configmap.yaml

# setup a Redis
kubectl apply -f deployments/gke-redis-master.yaml
kubectl apply -f deployments/gke-redis-slave.yaml

# setup Redis Service so Weather tracker can use it
kubectl apply -f deployments/gke-redis-slave-service.yaml
kubectl apply -f deployments/gke-redis-master-service.yaml

# Optional Load Balancer to work with it outside of Cloud
# kubectl apply -f deployments/gke-redis-master-loadbalancer.yaml

# deploy the weather tracker microservice
kubectl apply -f deployments/gke-deploy-weathertracker.yaml

# setup a service/load balancer to expose it to the world
kubectl apply -f deployments/gke-deploy-weathertracker-loadbalancer.yaml


