#!/bin/bash

## Daemon mode
# docker run \
#     -p 50505:50505 \
#     --name weathertrackerserver \
#     -e "WEATHERTRACKER_SERVER_PORT=50505" \
#    -d weathertracker:version1.0

## Interactive mode
docker run \
    -p 50505:50505 \
    --name weathertrackerserver \
    -e "WEATHERTRACKER_SERVER_PORT=50505" \
   weathertracker:version1.0