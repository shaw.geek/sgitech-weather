#!/bin/bash

external_ip=""; while [ -z $external_ip ]; do echo "Waiting for weathertrackerserver endpoint..."; external_ip=$(kubectl get svc weathertrackerserver --template="{{range .status.loadBalancer.ingress}}{{.ip}}{{end}}"); [ -z "$external_ip" ] && sleep 5; done; echo "Endpoint ready: $external_ip"; export endpoint=$external_ip
external_port=$(kubectl get svc weathertrackerserver --template="{{range .spec.ports}}{{.port}}{{end}}")
echo "Port: $external_port"

WEATHER_TRACKER_ADDRESS=$endpoint:$external_port
export WEATHER_TRACKER_ADDRESS
go run cmd/weather-client/main.go -add Hudson 1
go run cmd/weather-client/main.go -get Hudson C