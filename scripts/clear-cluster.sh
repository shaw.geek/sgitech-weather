#!/bin/bash

kubectl delete service -l app=weathertracker-loadbalancer
kubectl delete deployment -l app=weathertracker

# kubectl delete service redis-master-loadbalancer
kubectl delete service -l app=redis
kubectl delete deployment -l app=redis