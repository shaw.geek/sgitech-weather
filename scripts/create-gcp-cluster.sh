#!/bin/bash


# check if cluster exists
if gcloud container clusters list | grep -q sgitech-weather; then
    if $1 == "delete"; then #passed in delete, so delete it then recreate it
        gcloud container clusters delete sgitech-weather -q
    else
        echo "Cluster Exists - no action taken"
        exit 1
    fi
fi

# create the cluster - copied from CLI template off GCP dashboard
## Change to your project name or use an environment variable or pass it into this script as an arg
gcloud beta container \
--project "shawgeek-test" \
clusters create "sgitech-weather" \
--zone "us-central1-c" \
--no-enable-basic-auth \
--release-channel "rapid" \
--machine-type "g1-small" \
--image-type "COS" \
--disk-type "pd-standard" \
--disk-size "30" \
--metadata disable-legacy-endpoints=true \
--scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
--num-nodes "3" \
--enable-ip-alias \
--network "projects/shawgeek-test/global/networks/default" \
--subnetwork "projects/shawgeek-test/regions/us-central1/subnetworks/default" \
--default-max-pods-per-node "110" \
--no-enable-master-authorized-networks \
--addons HorizontalPodAutoscaling,HttpLoadBalancing \
--enable-autoupgrade \
--enable-autorepair