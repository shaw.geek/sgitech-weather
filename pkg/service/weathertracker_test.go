package service

import (
	"context"
	"testing"
	"time"

	pb "shawgeek.com/sgitech-weather/api/weathertracker"
	redismock "shawgeek.com/sgitech-weather/pkg/repository/mock"

	"github.com/golang/mock/gomock"
)

func TestWeatherTrackerService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create mock for the Redis Repo
	mockRepo := redismock.NewMockWeatherTrackerRepository(ctrl)
	// Create service
	svc := NewWeatherTracker(mockRepo)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	// Test Add Location Temp
	// Arrange
	mockRepo.
		EXPECT().
		Store(gomock.Any(), gomock.Any()).
		Return(nil)
	addreq := &pb.AddLocationTemperatureRequest{
		LocationName: "test",
		TemperatureK: 0,
	}
	// Act
	addreply, err := svc.AddLocationTemperature(ctx, addreq)
	// Assert
	if err != nil || addreply.Message == "" {
		t.Fatalf("Test failed: %v", err)
	}

	// Test Get Location Temp
	// Arrange
	mockRepo.
		EXPECT().
		Retrieve(gomock.Any()).
		Return(int32(274), nil) // 274K = 1C
	getreq := &pb.GetLocationTemperatureRequest{
		LocationName: "test",
		Scale:        pb.GetLocationTemperatureRequest_C,
	}
	// Act
	getreply, err := svc.GetLocationTemperature(ctx, getreq)
	// Assert
	if err != nil {
		t.Fatalf("Test failed: %v", err)
	}
	if getreply.Temperature != 1 {
		t.Fatalf("Test failed: getreply Temperature expecting [0] got [%d]", getreply.Temperature)
	}

}
