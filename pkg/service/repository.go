package service

// WeatherTrackerRepository operation interfaces for persistance
type WeatherTrackerRepository interface {
	Store(locationName string, temperatureK int32) error
	Retrieve(locationName string) (int32, error)
}
