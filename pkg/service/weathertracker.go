package service

import (
	"context"
	"errors"
	"fmt"
	"log"

	pb "shawgeek.com/sgitech-weather/api/weathertracker"
)

var (
	// ErrLocationNotFound : Location Not Found
	ErrLocationNotFound = errors.New("Location Not Found")
)

// WeatherTracker is used to implement WeatherTracker.WeatherTracker.
type WeatherTracker struct {
	pb.UnimplementedWeatherTrackerServer
	weatherTrackerRepository WeatherTrackerRepository
}

// NewWeatherTracker creates a new WeatherTracker with a WeatherTrackerRepository
func NewWeatherTracker(repo WeatherTrackerRepository) *WeatherTracker {
	log.Print("weathertracker.NewWeatherTracker\n")
	return &WeatherTracker{
		weatherTrackerRepository: repo,
	}
}

// AddLocationTemperature implements WeatherTracker
func (wt *WeatherTracker) AddLocationTemperature(ctx context.Context, in *pb.AddLocationTemperatureRequest) (*pb.AddLocationTemperatureReply, error) {
	locationName := in.GetLocationName()
	temperatureKelvin := in.GetTemperatureK()

	log.Printf("weathertracker.AddLocationTemperature Received: %v %v", locationName, temperatureKelvin)

	err := wt.weatherTrackerRepository.Store(locationName, temperatureKelvin)
	if err != nil {
		log.Printf("weathertracker.AddLocationTemperature Error Storing Location Temperature: %v\n", err)
		return nil, err
	}

	msg := fmt.Sprintf("Added Location %s with temperature %d", locationName, temperatureKelvin)
	reply := &pb.AddLocationTemperatureReply{
		Message: msg,
	}
	log.Printf("weathertracker.AddLocationTemperature Replying: %s", reply.Message)
	return reply, nil
}

// GetLocationTemperature implements WeatherTracker
func (wt *WeatherTracker) GetLocationTemperature(ctx context.Context, in *pb.GetLocationTemperatureRequest) (*pb.GetLocationTemperatureReply, error) {

	location := in.GetLocationName()
	scale := in.GetScale()
	log.Printf("weathertracker.GetLocationTemperature Received: %v %v", location, scale)

	temperatureKelvin, err := wt.weatherTrackerRepository.Retrieve(location)
	if err != nil {
		log.Printf("weathertracker. Error Retrieving Location Temperature: %v\n", err)
		return nil, err
	}
	// Convert Value for Scale
	temperature := convertKelvin(temperatureKelvin, scale)

	reply := &pb.GetLocationTemperatureReply{
		Temperature: temperature,
	}
	log.Printf("weathertracker.GetLocationTemperature Replying: %d", reply.Temperature)
	return reply, nil
}

func convertKelvin(temperatureK int32, scale pb.GetLocationTemperatureRequest_TemperatureScale) int32 {
	// Convert to Enum
	var temperature int32
	switch scale {
	case pb.GetLocationTemperatureRequest_C:
		temperature = temperatureK - 273 //273.15 to be precise but its intergers :)
	case pb.GetLocationTemperatureRequest_F:
		temperature = ((temperatureK - 273) * 9 / 5) + 32 //273.15 to be precise but its intergers :)
	case pb.GetLocationTemperatureRequest_K:
		temperature = temperatureK
	}
	return temperature
}
