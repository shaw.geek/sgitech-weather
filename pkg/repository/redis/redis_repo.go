package redis

import (
	"fmt"
	"log"
	"strconv"

	"shawgeek.com/sgitech-weather/pkg/service"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"
)

type redisRepository struct {
	client *redis.Client
}

func newRedisClient(redisURL string) (*redis.Client, error) {
	log.Printf("redis.newRedisClient- URL:[%s]\n", redisURL)
	opts, err := redis.ParseURL(redisURL)
	if err != nil {
		return nil, err
	}
	client := redis.NewClient(opts)
	_, err = client.Ping().Result()
	if err != nil {
		return nil, err
	}
	return client, nil
}

// NewRedisRepository return a new Redis Repo
func NewRedisRepository(redisURL string) (service.WeatherTrackerRepository, error) {
	log.Print("redis.NewRedisRepository\n")
	repo := &redisRepository{}
	client, err := newRedisClient(redisURL)
	if err != nil {
		return nil, errors.Wrap(err, "repository.NewRedisRepository")
	}
	repo.client = client
	log.Printf("NewRedisRepository Repo - [%T]:%+v", repo, repo)
	return repo, nil
}

func (r *redisRepository) generateKey(locationName string) string {
	return fmt.Sprintf("location:%s", locationName)
}

func (r *redisRepository) Retrieve(locationName string) (int32, error) {
	log.Print("redis.Retrieve\n")
	var temperatureKelvin int64
	key := r.generateKey(locationName)
	data, err := r.client.HGetAll(key).Result()
	if err != nil {
		return 0, errors.Wrap(err, "repository.WeatherTracker.Retrieve")
	}
	if len(data) == 0 {
		return 0, errors.Wrap(service.ErrLocationNotFound, "repository.WeatherTracker.Retrieve")
	}
	if err != nil {
		return 0, errors.Wrap(err, "repository.WeatherTracker.Retrieve")
	}
	temperatureKelvin, err = strconv.ParseInt(data["temperatureKelvin"], 10, 32)
	if err != nil {
		return 0, errors.Wrap(err, "repository.WeatherTracker.Retrieve")
	}
	return int32(temperatureKelvin), nil
}

func (r *redisRepository) Store(locationName string, temperatureK int32) error {
	log.Print("redis.Store\n")
	key := r.generateKey(locationName)
	data := map[string]interface{}{
		"locationName":      locationName,
		"temperatureKelvin": temperatureK,
	}
	_, err := r.client.HMSet(key, data).Result()
	if err != nil {
		return errors.Wrap(err, "repository.WeatherTracker.Store")
	}
	return nil
}
