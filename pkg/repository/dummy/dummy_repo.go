package dummy

import (
	"log"

	"shawgeek.com/sgitech-weather/pkg/service"
)

type dummyRepo struct {
}

// NewDummyRepository return a new dummy
func NewDummyRepository() (service.WeatherTrackerRepository, error) {
	log.Print("dummy.NewDummyRepository\n")
	repo := &dummyRepo{}
	return repo, nil
}

func (r *dummyRepo) Retrieve(locationName string) (int32, error) {
	log.Print("dummy.Retrieve\n")
	var temperatureKelvin int64
	return int32(temperatureKelvin), nil
}

func (r *dummyRepo) Store(locationName string, temperatureK int32) error {
	log.Print("dummy.Store\n")
	return nil
}
